/* -*- compile-command: "gcc -Wall -pedantic -g3 hash_prod.c -o hash_prod" -*- */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

unsigned int str_to_uint_hash(char *s , int len, uint8_t group_count)
{
    unsigned long hash = 5381;
    unsigned char *p;
    unsigned int i = 0;
    
    p = (unsigned char *) s;
    
    for(i = 0; i < len; i++)
    {
        if (*p == '\0') break;        
        hash = (hash << 5) + hash + *p;
        ++p;
    }
    return (unsigned int) (hash % group_count);
}

int main(int argc, char *argv[]) {
    
    FILE *fp, *new_fp;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    int idx = -1;

    fp = fopen("callid-sample-prod.txt", "r");
    if(fp == NULL)
        exit(EXIT_FAILURE);

    new_fp = fopen("hash_prod.txt", "w");
    if(fp == NULL)
        exit(EXIT_FAILURE);
        
    
    while((read = getline(&line, &len, fp)) != -1)
    {
        idx = str_to_uint_hash(line, (int)read, 2);
        fprintf(new_fp, "%d\n", idx);
    }

    fclose(fp);
    if(line) free(line);
    fclose(new_fp);
    
    return 0;
}
