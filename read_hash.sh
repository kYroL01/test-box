#!/bin/bash

zero=0
one=0
others=0

while IFS= read -r line; do
	if [ "$line" == 0 ]; then
		zero=$((zero+1))
	elif [ "$line" == 1 ]; then
		one=$((one+1))
	else
		others=$((others+1))
	fi
done < "$1"

echo "Zero idx = $zero"
echo "One idx = $one"
echo "Others idx = $others"

